<?php

class XmlToJson {
    public static function Parse ($url) {
        $fileContents= file_get_contents($url);
        $fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
        $fileContents = trim(str_replace('"', "'", $fileContents));
        $simpleXml = simplexml_load_string($fileContents);
        // $json = json_encode($simpleXml);

        return $simpleXml;
        // return $json;
    }
}

function transformTime($time) {
    $pattern = "/([\d]{4})([\d]{2})([\d]{2})([\d]{2})([\d]{2})([\d]{2})/";
    preg_match_all($pattern, $time, $matches);
    // $format = (string)$matches[4] . ':' . (string)$matches[5];
    return $matches[4][0] . ':' . $matches[5][0];
}

$date = (date('H') < 6 ? date('j') - 1 : date('j')) . '.' . date('n') . '.' . date('Y') . '.';

$channels = [1, 2, 3, 4, 185, 186, 310, 341];
$links = array();
foreach ($channels as $channel) {
    $links[] = 'http://mojtv.net/xmltv/service.ashx?kanal_id=' . $channel . '&date=' . $date;
}
$eve  = '[';
$i = 0;
foreach ($links as $link) {
    $data = XmlToJson::Parse($link);
     
    $eve .= ($i != 0 ? ',': '') . '{"channel": "' . $data->channel->{'display-name'}. '",';
    $eve .= '"program": [';
    $j = 0;
    foreach ($data->programme as $single) {
        $eve .= ($j != 0 ? ',': '') . ' {';
        foreach($single->attributes() as $k => $v) {
            if ($k != 'channel') {
                $eve .= '"' . $k . '": "' . transformTime($v) .'",';
            } else {
                $eve .= '"' . $k . '":"' . $v . '",';
            }
        }
        $eve .= '"title":"' . str_replace('"', '\"', $single->{'title'}) . '"';
        if ($single->{'category'} != '') {
            $eve .= ',"category":"' . $single->{'category'} . '"';
        } else if ($single->{'category'} == '' && strpos($single->{'title'}, 'dokumentar')) {
            $eve .= ',"category":"dokumentarni"';
        }
        if ($single->{'url'} != '') {
            $eve .= ',"url":"' . $single->{'url'} . '"'; 
        } 

        $eve .= '}';
        $j++;
    }
    $eve .= ']}';
    $i++;
}
$eve .= ']';

$jsonEve = json_decode($eve);
function findShowsNow($json) {
    $now = array();
    $currHour = date('H');
    $currMin = date('i');

    // var_dump ($json);
    // echo $single->start;
    $i = 0;
    foreach ($json as $single) {
        $j = 0;  
        // echo '<pre>';
        // // echo $j;
        // echo '</pre>';
        unset($tmp);
        $tmp = array();
        foreach ($single->program as $timeSlot) {
            $hr = explode(':', $timeSlot->start)[0];
            $min = explode(':', $timeSlot->start)[1];
            if ($hr == $currHour && $min <= $currMin) {
                $tmp[$j] = $timeSlot;
                if ($tmp[$j] == '') {
                    if ($hr == $currHour-1 && $min <= $currMin) {
                        $tmp[$j] = $timeSlot;
                        if ($tmp[$j] == '') {
                            if ($hr == $currHour-2 && $min <= $currMin) {
                                $tmp[$j] = $timeSlot;
                                if ($tmp[$j] == '') {
                                    if ($hr == $currHour-3 && $min <= $currMin) {
                                        $tmp[$j] = $timeSlot;
                                    } 
                                }
                            }
                        }
                    }
                } 
                $j++;
            } else if ($hr == ($currHour - 1)) {
                $tmp[$j] = $timeSlot;
                $j++;
            } else if ($hr == ($currHour - 2)) {
                $tmp[$j] = $timeSlot;
                $j++;
            } else if ($hr == ($currHour - 3)) {
                $tmp[] = $timeSlot;
                $j++;
            } else if ($currHour == 0) {
                if ($hr == ($currHour + 24 -1)) {
                    $tmp[$j] = $timeSlot;
                    if ($tmp[$j] == '') {
                        if ($hr == ($currHour + 24 -2)) {
                            $tmp[$j] = $timeSlot;
                        }
                    }
                }
            } 
        }
        if (count($tmp) !== 0) {
            $now[] = $tmp[count($tmp) - 1];
            
        } else {
            $now[] = '';
        }
        $i++;
    }
    return $now;
    $aa = $now;
    $textual = '[';
    $tmpI = 1;
    $tmpCnt = count($now);
    foreach ($now as $curr) {
        $tmpJ = 1;

        $textual .= '{';
        $scndTmpCnt = count(get_object_vars($curr));
        foreach ($curr as $key => $value) {
            $textual .= '"' . $key . '": "' . $value . '"';
            $textual .= ($tmpJ == $scndTmpCnt) ? '' : ',';
            $tmpJ++;
        }
        $textual .= '}';
        $textual .= ($tmpI == $tmpCnt) ? '' : ',';
        $tmpI++;
    } 
    $textual .= ']';
    return $aa;
}

function findShowsNext($json) {
    $next = array();
    $currHour = date('H');
    $currMin = date('i');

    // var_dump ($json);
    // echo $single->start;
    $i = 0;
    foreach ($json as $single) {
        $j = 0;  
        // echo '<pre>';
        // // echo $j;
        // echo '</pre>';
        unset($tmp);
        $tmp = array();
        foreach ($single->program as $timeSlot) {
            $hr = explode(':', $timeSlot->start)[0];
            $min = explode(':', $timeSlot->start)[1];
            if ($hr == $currHour && $min >= $currMin) {
                $tmp[$j] = $timeSlot;
                if ($tmp[$j] == '') {
                    if ($hr == $currHour+1 && $min >= $currMin) {
                        $tmp[$j] = $timeSlot;
                        if ($tmp[$j] == '') {
                            if ($hr == $currHour+2 && $min >= $currMin) {
                                $tmp[$j] = $timeSlot;
                                if ($tmp[$j] == '') {
                                    if ($hr == $currHour+3 && $min >= $currMin) {
                                        $tmp[$j] = $timeSlot;
                                    } 
                                }
                            }
                        }
                    }
                } 
                $j++;
            } else if ($hr == ($currHour + 1)) {
                $tmp[$j] = $timeSlot;
                $j++;
            } else if ($hr == ($currHour + 2)) {
                $tmp[$j] = $timeSlot;
                $j++;
            } else if ($hr == ($currHour + 3)) {
                $tmp[$j] = $timeSlot;
                $j++;
            }
        }
        // echo '<pre>';
        // var_dump($tmp);
        // echo '</pre>';
        // echo '<br>';
        // $next = $tmp;
        $next[] = $tmp[0];
        $i++;
    }
    
    return $next;
    $tmpI = 1;
    $tmpCnt = count($next);
    $sensual = '[';
    foreach ($next as $nextee) {
        $tmpJ = 1;
        $scndTmpCnt = count(get_object_vars($nextee));

        $sensual .= '{';
        foreach ($nextee as $key => $value) {
            $sensual .= '"' . $key . '": "' . $value . '"';
            $sensual .= ($tmpJ == $scndTmpCnt) ? '' : ',';
            $tmpJ++;
        }
        $sensual .= '}';
        $sensual .= ($tmpI == $tmpCnt) ? '' : ',';
        $tmpI++;        
    } 
    $sensual .= ']';
    
    return $bb;
}

function combine($now, $next) {
    // if (count($now) != count($next)) {
    //     return '';
    // }

    $combined = '"following": [';
    for ($i = 0; $i < count($now); $i++) {
        $combined .= '{"now": {';
        $j = 0;
        $cnt = count(get_object_vars($now[$i])) - 1;
        foreach ($now[$i] as $property => $value) {
            $combined .= '"' . $property . '": "' . $value . '"';
            $combined .= ($j == $cnt) ? '' : ',';
            $j++;

        }
        // foreach ($next as $key => $object) {
        //     foreach ($object as $property => $value) {
        //         $combined .= '"' . $property . '": "' . $value . '"';
        //         $combined .= ($j == $cnt) ? '' : ',';
        //         $j++;
        //     }
        // }
        $combined .= '}, "next": {';
        $j = 0;
        $cnt = count(get_object_vars($next[$i])) - 1;
        foreach ($next[$i] as $property => $value) {
            $combined .= '"' . $property . '": "' . $value . '"';
            $combined .= ($j == $cnt) ? '' : ',';
            $j++;
        }
        $combined .= '}}';
        $combined .= ($i == count($now) -1 ) ? '' : ',';
        // foreach ($next as $key => $object) {
        //     foreach ($object as $property => $value) {
        //         $combined .= '"' . $property . '": "' . $value . '"';
        //         $combined .= ($j == $cnt) ? '' : ',';
        //         $j++;
        //     }
        // }
        $combined .= '}]' . ($i == count($now)) ? '' : ',';
    }
    // $combined = '"following": { "now": ' . $now . ', "next": ' . $next . '}';
    $combined .= ']';
    return $combined;
}

$axiosRtrn = '{"channels":';
// $axiosRtrn .= $eve . ',' . '"now": ' . findShowsNow($jsonEve) . ', "next": ' . findShowsNext($jsonEve) . '}';
$axiosRtrn .= $eve . ',' . combine(findShowsNow($jsonEve), findShowsNext($jsonEve)) . '}';
echo $axiosRtrn;