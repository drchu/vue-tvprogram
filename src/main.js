import Vue from 'vue'

import axios from 'axios'
import VueAxios from 'vue-axios'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


import App from './App.vue'

Vue.config.productionTip = false
Vue.use(VueAxios, axios, BootstrapVue)

new Vue({
  render: h => h(App),
}).$mount('#app')
